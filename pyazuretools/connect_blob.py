# -*- coding: utf-8 -*-
"""
Created on Wed Oct  7 16:47:19 2020

@author: s.gnodde
"""

from azure.keyvault.secrets import SecretClient
from azure.identity import DefaultAzureCredential
from azure.storage.blob import BlobClient, ContainerClient, __version__
import os
import pandas as pd
import io


class BlobConnecterKV:
    """Connect to a blob storage via a secret from a key vault.
    """

    def __init__(self,
                 storage_account_key_secret_name,
                 storage_account_name_secret_name,
                 keyvault_name=None
                 ):
        """Setup blob connector.

        Parameters
        ----------
        storage_account_key_secret_name : str
            Secret name of storageAccountKey.
        storage_account_name_secret_name : str
            Secret name of storageAccountName.
        keyvault_name : str, optional
            Name of the key vault. The default is None.
            If None, tries to take it out of the environment
            variables under the name "KEY_VAULT_NAME".

        Raises
        ------
        ValueError
            If key vault name is not set, either in environment
            variables under "KEY_VAULT_NAME" or given as argument.

        Returns
        -------
        None.

        """
        # keyVaultName
        if keyvault_name is None:
            keyvault_name = os.environ["KEY_VAULT_NAME"]
        if keyvault_name is None:
            raise ValueError("keyVaultName not set.")
        self.KVUri = "https://" + keyvault_name + ".vault.azure.net"
        self.keyVaultName = keyvault_name

        self.credential = DefaultAzureCredential()
        self.client = SecretClient(
            vault_url=self.KVUri, credential=self.credential)

        self.SAkey = self.client.get_secret(storage_account_key_secret_name)
        self.SAname = self.client.get_secret(storage_account_name_secret_name)

        self.conn_str = f'DefaultEndpointsProtocol=https;AccountName={self.SAname.value};AccountKey={self.SAkey.value};EndpointSuffix=core.windows.net'  # noqa
        self.blob = None  # set in connect method

    def connect(self,
                container_name="raw",
                blob_name="debiet_draaiuur_test.csv"):
        """Connect to a blob.

        Parameters
        ----------
        container_name : str, optional
            Name of the container. The default is "raw".
        blob_name : str, optional
            Name of the blob. The default is "debiet_draaiuur_test.csv".

        Returns
        -------
        None.

        """
        self.blob = BlobClient\
            .from_connection_string(conn_str=self.conn_str,
                                    container_name=container_name,
                                    blob_name=blob_name)

        return self.blob

    def create_container(self, container_name):
        """Create a container in the blob.

        Parameters
        ----------
        container_name : str
            Name of the container.

        Returns
        -------
        None.

        """
        container_client = ContainerClient\
            .from_connection_string(conn_str=self.conn_str,
                                    container_name=container_name)
        container_client.create_container()

    def upload(self, filepath, blob_name, container_name):
        """Upload a file to the blob storage.

        Parameters
        ----------
        filepath : str
            Path of the file to upload.
        blob_name : str
            Name of the blob to make.
        container_name : str
            Name of the container.

        Returns
        -------
        None.

        """
        blob = BlobClient.from_connection_string(conn_str=self.conn_str,
                                                 container_name=container_name,
                                                 blob_name=blob_name)

        with open(filepath, "rb") as data:
            blob.upload_blob(data)

    def download(self, filepath):
        """Download from blob storage. First connect with self.connect.

        Parameters
        ----------
        filepath : str
            Filepath where to save new file.

        Returns
        -------
        None.

        """
        with open(filepath, "wb") as my_blob:
            blob_data = self.blob.download_blob()
            blob_data.readinto(my_blob)

    def read_from_feather(self):
        assert self.blob.blob_name.endswith(
            '.feather'), 'This is not a feather file, use another method'
        blob_data = self.blob.download_blob()
        data_frame = pd.read_feather(io.BytesIO(blob_data.content_as_bytes()))

        return data_frame


def test_installation():
    try:
        print("Azure Blob storage v" + __version__)
    except Exception as ex:
        print('Exception:')
        print(ex)


def read_feather_from_blob(container_name, blob_name):
    """ read feather files from Azure blob storage
    Parameters
    ---------------
    container_name: str
        name of the blob storage container
    blob_name: str
        name of the file, shoudl be a feather file

    """
    bc = BlobConnecterKV(keyvault_name='kvvwam')
    bc.connect(container_name=container_name, blob_name=blob_name)
    df = bc.read_from_feather()

    return df
