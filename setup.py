import setuptools
setuptools.setup(
    name="pyazuretools",
    version="1.0.0",
    author="Sjoerd Gnodde",
    author_email="s.gnodde@hetwaterschapshuis.nl",
    description="A package with a number of helpful tools for Azure and Python",
    url="https://dev.azure.com/HWH-WE-DEEP/DEEP-TempRepository/_git/PyAzureTools",
    packages=setuptools.find_packages(),
    python_requires='>=3.6',
    install_requires=[
        'azure-keyvault',
        'azure-identity',
        'pandas',
        'azure-storage-blob'
    ]
)
