# Introduction 
This is a collection of Azure Tools written during and for the DEEP projects, but may have other uses. 
At this moment, this is a 

# Getting Started
```python
! git clone https://HWH-WE-DEEP@dev.azure.com/HWH-WE-DEEP/DEEP-TempRepository/_git/PyAzureTools
! pip install -e .
```
You might need to login. The easiest method is to use a Personal Access Token instead. Fill in this token as your password.

# Modules
The following modules have been added so far:
- `connect_blob`: connect to a Blob storage


# Test
No tests implemented yet.

# Contribute
If you want to contribute to this package, pleas send a pull request with you proposed addition/change and set @s.gnodde@hetwaterschapshuis.nl
as a required reviewer.
